# Essai de déploiement local d'app Node.js sur minikube

- gitlab-ci-local
- registre docker local
- minikube

## Setup

### Docker/minikube

Voir [ansible](ansible/README.md) pour le setup de la VM avec Docker et minikube.

### Webhook

Config la NAT de externe 8000 vers interne 9000 sur l'IP de la VM sur le LAN.

Webhook créé sur GitLab CI (pipeline event) avec l'URL http://212.195.32.155:8000/hooks/redeploy-webhook

```
sudo apt-get install -y webhook
```

Créer `/etc/webhook.conf` (contenu du `hooks.json`) :

`object_attributes.sha`
`object_attributes.tag`
`object_attributes.ref`
`object_attributes.created_at`
`object_attributes.finished_at`
`object_attributes.status`

```json
[
  {
    "id": "redeploy-webhook",
    "execute-command": "/home/debian/redeploy-webhook.sh",
    "command-working-directory": "/home/debian",
    "pass-arguments-to-command": [
      {
        "source": "payload",
        "name": "user.username"
      },
      {
        "source": "payload",
        "name": "object_attributes.sha"
      },
      {
        "source": "payload",
        "name": "object_attributes.tag"
      },
      {
        "source": "payload",
        "name": "object_attributes.ref"
      },
      {
        "source": "payload",
        "name": "object_attributes.created_at"
      },
      {
        "source": "payload",
        "name": "object_attributes.finished_at"
      },
      {
        "source": "payload",
        "name": "object_attributes.status"
      }
    ],
    "response-message": "Executing redeploy script",
    "trigger-rule": {
      "and": [
        {
          "match": {
            "type": "value",
            "value": "success",
            "parameter": {
              "source": "payload",
              "name": "object_attributes.status"
            }
          }
        },
        {
          "match": {
            "type": "value",
            "value": "YOUR-TOKEN-HERE",
            "parameter": {
              "source": "header",
              "name": "X-Gitlab-Token"
            }
          }
        }
      ]
    }
  }
]
```

Créer `/etc/systemd/system/webhook.service` mais **attention** voici ce qu'on trouve après démarrage

```
$ ps aux | grep webhook
root      127969  0.0  0.2 1163728 8416 ?        Ssl  02:12   0:00 /usr/bin/webhook -nopanic -hooks /etc/webhook.conf
debian    130693  0.0  0.0   6332  2116 pts/0    S+   02:16   0:00 grep webhook
```

Le `ExecStart` semble être ignoré.

```
[Service]
ExecStart=/usr/bin/webhook -hooks /etc/webhook.conf -verbose -port 9000
User=debian
Group=debian
```

Créer `/home/debian/redeploy-webhook.sh` avec ce contenu (exemple) :

```
#!/bin/bash

DATE=$(date)
echo "Webhook was called on $DATE" >> log.txt
```

### Exemple de payload entrante :

Utiliser :

`object_attributes.sha`
`object_attributes.ref`
`object_attributes.tag`
`object_attributes.created_at`
`object_attributes.finished_at`
`object_attributes.status`
`user.username`

```json
{
  "object_kind": "pipeline",
  "object_attributes": {
    "id": 1260900407,
    "iid": 10,
    "name": null,
    "ref": "v0.0.1",
    "tag": true,
    "sha": "e10b758ea25235e6070a82017d04a72e113a6b81",
    "before_sha": "0000000000000000000000000000000000000000",
    "source": "push",
    "status": "failed",
    "detailed_status": "failed",
    "stages": [
      "backend_install",
      "backend_lint",
      "backend_build",
      "backend_build_image",
      "frontend_install",
      "frontend_lint",
      "frontend_build"
    ],
    "created_at": "2024-04-20 00:37:18 UTC",
    "finished_at": "2024-04-20 00:39:03 UTC",
    "duration": 101,
    "queued_duration": null,
    "variables": [],
    "url": "https://gitlab.com/bhubert/local-k8s-deployment-test/-/pipelines/1260900407"
  },
  "merge_request": null,
  "user": {
    "id": 1145117,
    "name": "Benoît Hubert",
    "username": "bhubert",
    "avatar_url": "https://secure.gravatar.com/avatar/d473f42a99388e7a6795516c1fe3f2e32f3a026d45a3eb4bb734cc34a00568e9?s=80&d=identicon",
    "email": "[REDACTED]"
  },
  "project": {
    "id": 57044569,
    "name": "local-k8s-deployment-test",
    "description": null,
    "web_url": "https://gitlab.com/bhubert/local-k8s-deployment-test",
    "avatar_url": null,
    "git_ssh_url": "git@gitlab.com:bhubert/local-k8s-deployment-test.git",
    "git_http_url": "https://gitlab.com/bhubert/local-k8s-deployment-test.git",
    "namespace": "Benoît Hubert",
    "visibility_level": 20,
    "path_with_namespace": "bhubert/local-k8s-deployment-test",
    "default_branch": "master",
    "ci_config_path": ""
  },
  "commit": {
    "id": "e10b758ea25235e6070a82017d04a72e113a6b81",
    "message": "Write playbook for minikube install\n",
    "title": "Write playbook for minikube install",
    "timestamp": "2024-04-19T22:49:44+02:00",
    "url": "https://gitlab.com/bhubert/local-k8s-deployment-test/-/commit/e10b758ea25235e6070a82017d04a72e113a6b81",
    "author": {
      "name": "Benoît Hubert",
      "email": "benoithubert@gmail.com"
    }
  },
  "builds": [
    {
      "id": 6673449889,
      "stage": "frontend_build",
      "name": "frontend_build",
      "status": "skipped",
      "created_at": "2024-04-20 00:37:18 UTC",
      "started_at": null,
      "finished_at": null,
      "duration": null,
      "queued_duration": null,
      "failure_reason": null,
      "when": "on_success",
      "manual": false,
      "allow_failure": false,
      "user": [null],
      "runner": null,
      "artifacts_file": [null],
      "environment": null
    },
    {
      "id": 6673449887,
      "stage": "frontend_install",
      "name": "frontend_install",
      "status": "skipped",
      "created_at": "2024-04-20 00:37:18 UTC",
      "started_at": null,
      "finished_at": null,
      "duration": null,
      "queued_duration": null,
      "failure_reason": null,
      "when": "on_success",
      "manual": false,
      "allow_failure": false,
      "user": [null],
      "runner": null,
      "artifacts_file": [null],
      "environment": null
    },
    {
      "id": 6673449883,
      "stage": "backend_install",
      "name": "backend_install",
      "status": "success",
      "created_at": "2024-04-20 00:37:18 UTC",
      "started_at": "2024-04-20 00:37:19 UTC",
      "finished_at": "2024-04-20 00:37:41 UTC",
      "duration": 21.733804,
      "queued_duration": 0.302245,
      "failure_reason": null,
      "when": "on_success",
      "manual": false,
      "allow_failure": false,
      "user": [null],
      "runner": [null],
      "artifacts_file": [null],
      "environment": null
    },
    {
      "id": 6673449884,
      "stage": "backend_lint",
      "name": "backend_lint",
      "status": "success",
      "created_at": "2024-04-20 00:37:18 UTC",
      "started_at": "2024-04-20 00:37:41 UTC",
      "finished_at": "2024-04-20 00:38:01 UTC",
      "duration": 20.018926,
      "queued_duration": 0.194723,
      "failure_reason": null,
      "when": "on_success",
      "manual": false,
      "allow_failure": false,
      "user": [null],
      "runner": [null],
      "artifacts_file": [null],
      "environment": null
    },
    {
      "id": 6673449885,
      "stage": "backend_build",
      "name": "backend_build",
      "status": "success",
      "created_at": "2024-04-20 00:37:18 UTC",
      "started_at": "2024-04-20 00:38:02 UTC",
      "finished_at": "2024-04-20 00:38:22 UTC",
      "duration": 19.420383,
      "queued_duration": 1.058439,
      "failure_reason": null,
      "when": "on_success",
      "manual": false,
      "allow_failure": false,
      "user": [null],
      "runner": [null],
      "artifacts_file": [null],
      "environment": null
    },
    {
      "id": 6673449888,
      "stage": "frontend_lint",
      "name": "frontend_lint",
      "status": "skipped",
      "created_at": "2024-04-20 00:37:18 UTC",
      "started_at": null,
      "finished_at": null,
      "duration": null,
      "queued_duration": null,
      "failure_reason": null,
      "when": "on_success",
      "manual": false,
      "allow_failure": false,
      "user": [null],
      "runner": null,
      "artifacts_file": [null],
      "environment": null
    },
    {
      "id": 6673449886,
      "stage": "backend_build_image",
      "name": "backend_build_image",
      "status": "failed",
      "created_at": "2024-04-20 00:37:18 UTC",
      "started_at": "2024-04-20 00:38:22 UTC",
      "finished_at": "2024-04-20 00:39:02 UTC",
      "duration": 40.058794,
      "queued_duration": 0.312643,
      "failure_reason": "script_failure",
      "when": "on_success",
      "manual": false,
      "allow_failure": false,
      "user": [null],
      "runner": [null],
      "artifacts_file": [null],
      "environment": null
    }
  ]
}
```
