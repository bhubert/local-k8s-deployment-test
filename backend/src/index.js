import fs from 'node:fs';
import express from 'express';

// uncomment line below to trigger linting error in CI
// import path from 'node:path';

const packageJSON = JSON.parse(fs.readFileSync('package.json', 'utf-8'));
console.log("app has dependencies:", packageJSON.dependencies);

const app = express();

app.get('/', (req, res) => res.send(packageJSON));

const port = process.env.PORT || 3000;
app.listen(port, () => console.log(`listening on ${port}`));
