# Utiliser Ansible pour installer et configurer Docker et minikube sur Debian

## Pré-requis

- Installer Debian 12 (dans mon cas, avec une ISO "unattended")
- voir l'IP : `ip route` pour voir l'IP
- Installer `sudo` (déjà fait avec mon ISO "unattended")
- Renommer la VM : `sudo hostnamectl hostname debian-minikube`

## Lancer le playbook Docker

```
ansible-playbook -i inventory.ini playbook-docker.yml -bkK
```

## Lancer le playbook minikube

```
ansible-playbook -i inventory.ini playbook-minikube.yml -bkK
```

## Se connecter en SSH pour lancer minikube

```
ssh debian@192.168.1.106
sudo minikube start --driver=none
```

**ATTENTION** fait quelques essais, mais je n'ai rien sur 6443 quand je lance le cluster avec `--driver=none`.



### Copie du playbook Docker

```yml
---
- name: Setup Docker and minikube on Debian
  hosts: debian-minikube
  become: yes
  tasks:
    - name: Update apt cache
      apt:
        update_cache: yes
    - name: Install CA Certificates and Curl
      apt:
        name:
          - ca-certificates
          - curl
        state: present
    - name: Create keys directory
      file:
        path: /etc/apt/keyrings
        state: directory
        mode: "0755"
    - name: Download Docker GPG key
      get_url:
        url: https://download.docker.com/linux/debian/gpg
        dest: /etc/apt/keyrings/docker.asc
        mode: "0644"
    # - name: Set permissions for Docker GPG key
    #   file:
    #     path: /etc/apt/keyrings/docker.asc
    #     mode: '0644'
    - name: Store architecture in a variable
      command: "dpkg --print-architecture"
      register: dpkg_print_arch_result
    - name: Add Docker repository to sources.list.d
      lineinfile:
        path: /etc/apt/sources.list.d/docker.list
        line: "deb [arch={{ dpkg_print_arch_result.stdout }} signed-by=/etc/apt/keyrings/docker.asc] https://download.docker.com/linux/debian {{ ansible_distribution_release }} stable"
        create: yes
    - name: Update apt caches
      apt:
        update_cache: yes
    - name: Install Docker
      apt:
        name: "{{ item }}"
        state: present
      with_items:
        - docker-ce
        - docker-ce-cli
        - containerd.io
        - docker-buildx-plugin
        - docker-compose-plugin
    - name: Add user to Docker group
      user:
        name: debian
        groups: docker
        append: yes
```

## Résumé fait au fur et à mesure

- installer conntrack avec apt-get
- installer crictl
  - download une release ici : https://github.com/kubernetes-sigs/cri-tools/releases
  - décompresser et mettre crictl sous /usr/local/bin
- installer cri-dockerd : https://github.com/Mirantis/cri-dockerd
  - download https://raw.githubusercontent.com/Mirantis/cri-dockerd/master/packaging/systemd/cri-docker.socket
  - download https://raw.githubusercontent.com/Mirantis/cri-dockerd/master/packaging/systemd/cri-docker.socket
- installer containernetworking-plugins : https://minikube.sigs.k8s.io/docs/faq/#how-do-i-install-containernetworking-plugins-for-none-driver
  - download: wget https://github.com/containernetworking/plugins/releases/download/v1.4.1/cni-plugins-linux-arm64-v1.4.1.tgz
  - unpack : sudo tar xvfz cni-plugins-linux-arm64-v1.4.1.tgz -C /opt/cni/bin

## as debian

```
sudo apt-get update
sudo apt-get install ca-certificates curl
sudo install -m 0755 -d /etc/apt/keyrings
sudo curl -fsSL https://download.docker.com/linux/debian/gpg -o /etc/apt/keyrings/docker.asc
sudo chmod a+r /etc/apt/keyrings/docker.asc
echo   "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.asc] https://download.docker.com/linux/debian \
  $(. /etc/os-release && echo "$VERSION_CODENAME") stable" |   sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt-get update
docker
sudo apt-get install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin
docker ps
sudo service docker start
gitlab-ci-local
sudo groupadd docker
sudo usermod -aG docker
sudo usermod -aG docker debian
docker ps
cd local-k8s-deployment-test/
gitlab-ci-local
git pull
gitlab-ci-local
cat .gitlab-ci.yml
git status
git diff
git pull
gitlab-ci-local
git pull
gitlab-ci-local
git pull
gitlab-ci-local
docker login
docker login help
docker login -u benoithubert -p dckr_pat_f83sAzQorb4tFYooBa6kQUSt2Ks
git status
git pull
gitlab-ci-local
cd ..
curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube_latest_amd64.deb
sudo dpkg -i minikube_latest_amd64.deb
minikube start --driver=none
sudo su -
wget https://github.com/kubernetes-sigs/cri-tools/releases/download/v1.30.0/crictl-v1.30.0-linux-amd64.tar.gz
tar tvzf crictl-v1.30.0-linux-amd64.tar.gz
tar xvzf crictl-v1.30.0-linux-amd64.tar.gz
ls -lh
sudo mv crictl /usr/local/bin/
minikube start --driver=none
sudo su -
```

## as root

```
minikube start --driver=none
apt-get install -y conntrack
minikube start --driver=none
minikube start --driver=none
minikube start --driver=none
cd
wget https://github.com/Mirantis/cri-dockerd/releases/download/v0.3.12/cri-dockerd-0.3.12.amd64.tgz
tar tvzf cri-dockerd-0.3.12.amd64.tgz
tar xvzf cri-dockerd-0.3.12.amd64.tgz
cd cri-dockerd
mkdir -p /usr/local/bin
install -o root -g root -m 0755 cri-dockerd /usr/local/bin/cri-dockerd
install packaging/systemd/* /etc/systemd/system
sed -i -e 's,/usr/bin/cri-dockerd,/usr/local/bin/cri-dockerd,' /etc/systemd/system/cri-docker.service
systemctl daemon-reload
systemctl enable --now cri-docker.socket
install -o root -g root -m 0755 cri-dockerd /usr/local/bin/cri-dockerd
install packaging/systemd/* /etc/systemd/system
ed -i -e 's,/usr/bin/cri-dockerd,/usr/local/bin/cri-dockerd,' /etc/systemd/system/cri-docker.service
sed -i -e 's,/usr/bin/cri-dockerd,/usr/local/bin/cri-dockerd,' /etc/systemd/system/cri-docker.service
systemctl enable --now cri-docker.socket
systemctl daemon-reload
cd
ls
ls
ls cri-dockerd
ls /etc/systemd/system
wget https://raw.githubusercontent.com/Mirantis/cri-dockerd/master/packaging/systemd/cri-docker.service
wget https://raw.githubusercontent.com/Mirantis/cri-dockerd/master/packaging/systemd/cri-docker.socket
ls
mv cri-docker.s* cri-docker
mv cri-docker.s* cri-dockerd
ls
cd cri-dockerd/
ls
ls -l
ls
ls -l
cat cri-docker.service
ls /etc/systemd/system/
ls
mkdir -p packaging/systemd
ls
mv cri-docker.s* packaging/systemd/
install packaging/systemd/* /etc/systemd/system
sed -i -e 's,/usr/bin/cri-dockerd,/usr/local/bin/cri-dockerd,' /etc/systemd/system/cri-docker.service
systemctl daemon-reload
systemctl enable --now cri-docker.socket
systemctl start cri-docker.socket
cd ..
minikube start --driver=none
CNI_PLUGIN_VERSION="1.4.1"
CNI_PLUGIN_TAR="cni-plugins-linux-amd64-$CNI_PLUGIN_VERSION.tgz"
CNI_PLUGIN_INSTALL_DIR="/opt/cni/bin"
curl -LO "https://github.com/containernetworking/plugins/releases/download/$CNI_PLUGIN_VERSION/$CNI_PLUGIN_TAR"
sudo mkdir -p "$CNI_PLUGIN_INSTALL_DIR"
sudo tar -xf "$CNI_PLUGIN_TAR" -C "$CNI_PLUGIN_INSTALL_DIR"
ls -ltrh
cat cni-plugins-linux-amd64-1.4.1.tgz
rm cni-plugins-linux-amd64-1.4.1.tgz
wget https://github.com/containernetworking/plugins/releases/download/v1.4.1/cni-plugins-linux-arm64-v1.4.1.tgz
ks
ls
mkdir /opt/cni/bin
sudo tar -xfz cni-plugins-linux-arm64-v1.4.1.tgz -C /opt/cni/bin
tar tvzf cni-plugins-linux-arm64-v1.4.1.tgz
sudo tar xvfz cni-plugins-linux-arm64-v1.4.1.tgz -C /opt/cni/bin
ls -l /opt/cni/bin/
minikube start --driver=none
minikube
minikube default
minikube status
docker ps
df -h
```
